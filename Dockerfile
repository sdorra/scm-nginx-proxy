FROM nginx:1.7.8
MAINTAINER Sebastian Sdorra <s.sdorra@gmail.com>

RUN apt-get update \
    && apt-get install -y -q --no-install-recommends ca-certificates openssl wget \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

ADD nginx/default.conf /etc/nginx/conf.d/default.conf
ADD nginx/default_ssl.conf /etc/nginx/conf.d/default_ssl.conf
ADD docker_entrypoint.sh /docker_entrypoint.sh

EXPOSE 80 443
CMD /docker_entrypoint.sh